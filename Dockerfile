FROM python:3

COPY . main
WORKDIR main
RUN pip install -r requirements.txt
EXPOSE 5000

CMD ["gunicorn", "run:app", "--bind=0.0.0.0:5000", "--workers=4"]
