"""
 Simple API endpoint for returning hello
"""
from flask import (Blueprint, render_template, current_app, request,  
                   flash, url_for, redirect, session, abort, jsonify, make_response)
from .version import version_name

hello = Blueprint(version_name+"_hello",__name__, url_prefix='/hello')

@hello.route('/', methods=['GET'])
def index():

   data = {
      'message' : "Hello World! v1"
   }      
   return make_response(jsonify(data))