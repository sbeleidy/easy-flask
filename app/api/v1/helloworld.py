"""
 Simple API endpoint for returning helloworld
"""
from flask import (Blueprint, render_template, current_app, request,  
                   flash, url_for, redirect, session, abort, jsonify, make_response)
from .version import version_name

helloworld = Blueprint(version_name + "_hello_world", __name__, url_prefix='/helloworld')

@helloworld.route('/', methods=['GET'])
def index():

   data = {
      'message' : "Hello World!"
   }      
   return make_response(jsonify(data))