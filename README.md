# Easy Flask 

A boilerplate flask application with simple dockerization, testing,and deployment.

Directory structure influenced by http://blog.sampingchuang.com/flask-hello-world/


## Development Tips

**To add a new API blueprint:**

1. create a new file in the API version you're working on following the same style as hello.py
1. In that API version folder's `__init__.py` make sure you import the file's blueprint and add it to the version variable.

**To add a new API version:**

1. copy a version folder
1. edit the version.py file with the new version name
1. edit the version folder's `__init__.py` with the new blueprint list name
1. edit `api/__init__.py` to import the blueprint list
1. edit `app.py`'s api imports to import the new version and update the DEFAULT_BLUEPRINTS variable accordingly